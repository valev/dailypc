package pc84

import (
	"sync"
)

func main() {
	var wg sync.WaitGroup

	wg.Add(1)

	go listen()

	connect("localhost:9999")

    sayhi()

	wg.Wait()
}
