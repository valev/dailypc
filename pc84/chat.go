package pc84

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
)

type friend struct {
	name     string
	endpoint string
	conn     net.Conn
}

var friends = make([]friend, 0)

func listen() {
	l, err := net.Listen("tcp", "localhost:9999")
	if err != nil {
		log.Println(err)
	}
	defer l.Close()
	for {
		in, err := l.Accept()
		if err != nil {
			log.Println(err)
		}
		go func(c net.Conn) {
			defer c.Close()
			io.Copy(os.Stdout, c)
		}(in)
	}
}

func connect(endpoint string) {
	c, err := net.Dial("tcp", endpoint)
	if err != nil {
		log.Println(err)
		return
	}

	friends = append(friends, friend{"", endpoint, c})
}

func sayhi() {
	for _, f := range friends {
		fmt.Fprintf(f.conn, "HI")
	}
}
